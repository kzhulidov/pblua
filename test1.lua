function setupI2C(port)
  nxt.InputSetType(port,0)
  nxt.InputSetType(port,11)
  nxt.InputSetDir(port,1,1)
  nxt.InputSetState(port,1,1)

  nxt.I2CInitPins(port)
end

-- Now put the standard I2C messages into the nxt table
nxt.I2Cversion    = string.char( 0x02, 0x00 )
nxt.I2Cproduct    = string.char( 0x02, 0x08 )
nxt.I2Ctype       = string.char( 0x02, 0x10 )
nxt.I2Ccontinuous = string.char( 0x02, 0x41, 0x02 )
nxt.I2Cdata       = string.char( 0x02, 0x42 )

-- waitI2C() - sits in a tight loop until the I2C system goes idle

function waitI2C( port )
  while( 0 ~= nxt.I2CGetStatus( port ) ) do
  end
end

-- Put it all together in a function that prints out a report of which
-- sensor is connected to a port

function checkI2C(port)
  setupI2C(port)

  nxt.I2CSendData( port, nxt.I2Cversion, 8 )
  waitI2C( port )
  print( "Version    -> " .. nxt.I2CRecvData( port, 8 ) )

  nxt.I2CSendData( port, nxt.I2Cproduct, 8 )
  waitI2C( port )
  print( "Product ID -> " .. nxt.I2CRecvData( port, 8 ) )

  nxt.I2CSendData( port, nxt.I2Ctype, 8 )
  waitI2C( port )
  print( "SensorType -> " .. nxt.I2CRecvData( port, 8 ))
end

-- And now run the function to see if it recognizes the sensor
checkI2C(4)

function getData(port)
  local x = 0
  nxt.I2CSendData(port,string.char( 0x02, 0x41,0x01),0)
  waitI2C( port )
  for i = 1, 1000 do
    x = x + 1
  end
  --nxt.I2CSendData(port,string.char( 0x02, 0x41),1)
  --waitI2C( port )
  --for i = 1, 1000 do
  --  x = x + 1
  --end
  --local s=nxt.I2CRecvData(port,1)
  --print(string.format( "State: %3i", string.byte(s,1,1)))
  nxt.I2CSendData(port,nxt.I2Cdata, 8 )
  waitI2C( port )
  for i = 1, 1000 do
    x = x + 1
  end
  s=nxt.I2CRecvData(port,8)
  c1,c2,c3,c4,c5,c6,c7,c8 = string.byte(s,1,8)
  print( string.format( "Result: %3i %3i %3i %3i %3i %3i %3i %3i",c1, c2, c3, c4, c5, c6, c7, c8 ) )
end



function RedSensor(port)

  nxt.InputSetType(port,14)

  repeat
      local val = 0
      for i = 1,1000 do
         _,_,_,int = nxt.InputGetStatus(port)
         val = val + int
      end
      val = val / 1000
      print(val)

  until( 8 == nxt.ButtonRead() )
end
