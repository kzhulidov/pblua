if not nxt then
	require ('common.nxt')

	function nxt.BtStreamRecv()
		return '!!! +0.123011 -0.987033 0'
	end
end

function waitMs(delay)
	local start = nxt.TimerRead()
	repeat
		--coroutine.yield()
	until (nxt.TimerRead() - start) >= delay
end

function shoot()
	local port = 1
	nxt.OutputSetRegulation(port, 1, 1)
	nxt.OutputSetSpeed(port, 0x20, 50, 360)
end

function move(port, degrees)
  nxt.OutputSetRegulation(port,1,1)

  _,tacho = nxt.OutputGetStatus(port)
  nxt.OutputSetSpeed(port,0x20,nxt.sign(degrees)*50,nxt.abs(degrees))

  repeat
	_,curtacho = nxt.OutputGetStatus(port)
  until 4 > nxt.abs( curtacho - (tacho + degrees) )
end

function MotorSync(s, t)

	nxt.OutputResetTacho(2,1,1,1)
	nxt.OutputResetTacho(3,1,1,1)

	nxt.OutputSetRegulation(2,2,1)
	nxt.OutputSetRegulation(3,2,1)

	nxt.DisableNXT( 1 );
	nxt.OutputSetSpeed(2,0x20,s, 0, t )
	nxt.OutputSetSpeed(3,0x20,s, 0, t )
	nxt.DisableNXT( 0 );

end

function BtIdleWait()
	local active
	repeat
	  _,active = nxt.BtGetStatus()
	until 17 == active
end

function BtIsConnected()
	local state = nxt.BtGetStatus()
	return nxt.band(state, 2) == 2
end

function BtWaitConnection()
	print('Powering on BT ...')
	nxt.BtPower(1)
	BtIdleWait()
	print('Setting PIN every 3s ...')
	repeat
		nxt.BtSetPIN('1234')
		BtIdleWait()
		waitMs(3000)
		local state = nxt.BtGetStatus()
	until nxt.band(state, 2) == 2
	BtIdleWait()
	print('Connected!')
	nxt.BtStreamMode(1)
	BtIdleWait()
end

function BtDisconnect()
	if BtIsConnected() then
		nxt.BtDisconnectAll()
		--BtIdleWait()
	end
end

function BtReadNum(num, data)
	local str = data or ''
	repeat
		local d = nxt.BtStreamRecv()
		if d then
			str = str .. d
		elseif not BtIsConnected() then
			return nil
		end
	until #str >= num
	return string.sub(str, 1, num), string.sub(str, num+1)
end

function BtReadSync(data)
	local c
	repeat
		c, data = BtReadNum(3, data)
	until (not c) or (c == '!!!')
	if c then
		c, data = BtReadNum(25-3, data)
	end
	return data
end

function getFloat(str)
	local int = tonumber(string.sub(str, 1, 2))
	local float = tonumber(string.sub(str, 4))
	if int and float then
		local sign = string.sub(str, 1, 1)
		if sign == '-' then
			sign = -1
		else
			sign = 1
		end
		return nxt.float(int, float * sign)
	end
end

function main()

	local cmd, data

	nxt.OutputResetTacho(2,1,1,1)
	nxt.OutputResetTacho(3,1,1,1)
	nxt.OutputSetRegulation(2,1,1)
	nxt.OutputSetRegulation(3,1,1)

	BtWaitConnection()
	data = BtReadSync(data)

	repeat

		cmd, data = BtReadNum(25, data)

		if cmd then
			local num_x = getFloat(string.sub(cmd, 5, 5+9-1))
			local num_y = getFloat(string.sub(cmd, 15, 15+9-1))

			if num_x and num_y then
				local power_b = nxt.int( - nxt.floor(num_y * 50) - nxt.floor(num_x * 40) )
				local power_c = nxt.int( - nxt.floor(num_y * 50) + nxt.floor(num_x * 40) )

				nxt.OutputSetSpeed(2, 0x20, power_b)
				nxt.OutputSetSpeed(3, 0x20, power_c)
			end
			if string.sub(cmd, 25) == '1' then
				shoot()
			end
		end

	until (8 == nxt.ButtonRead()) or (not cmd)

	nxt.OutputSetSpeed(2)
	nxt.OutputSetSpeed(3)
	BtDisconnect()
end

main()