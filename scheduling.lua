function getTimer()
	return os.time() * 1000
end

function waitMs(delay)
	coroutine.yield(getTimer() + delay)
end

function func1()
	print('func1')
end

function func2()
	print('func2')
	waitMs(2000)
	print('func2-1')
end

function addTask(ct, func, period)
	local task = {}
	task.func = func
	task.co = coroutine.create(task.func)
	task.period = period
	task.offset = getTimer()
	table.insert(ct, task)
end

function runTasks(ct, last_idx)
	for i = 1, last_idx do
		local task = ct[i]
		if getTimer() >= task.offset then
			local res, offset = coroutine.resume(task.co)
			if coroutine.status(task.co) == "dead" then
				task.co = coroutine.create(task.func)
				task.offset = task.offset + task.period
			elseif res and offset then
				task.offset = offset
			end
			runTasks(ct, i)
		end
	end
end

function main()
	local ct = {}
	addTask(ct, func1, 1000)
	addTask(ct, func2, 5000)
	while true do
		runTasks(ct, 2)
	end
end

main()

