if not nxt then
	require ('common.nxt')
end

function waitMs(delay)
	local start = nxt.TimerRead()
	repeat
		--coroutine.yield()
	until (nxt.TimerRead() - start) >= delay
end

function moveStart(port, degrees)
	nxt.OutputSetRegulation(port, 1, 1, 4, 1, 15)
	nxt.OutputSetSpeed(port, 0x20, nxt.sign(degrees)*20, nxt.abs(degrees))
end

function moveWait(port, delay)
	local delay = delay or 10000
	local end_timer = nxt.TimerRead() + delay
	local tacho, overload, torun
	local ovl_count = 10
	repeat
		_,tacho,_,_,overload,_,torun = nxt.OutputGetStatus(port)
		if 1 == overload then
			ovl_count = ovl_count - 1
		end
	until 2 > torun or 0 > ovl_count or nxt.TimerRead() >= end_timer
	nxt.OutputSetSpeed(port, 0x60)
	return tacho
end

function move(port, degrees)
	moveStart(port, degrees)
	return moveWait(port)
end

arm = {}

function arm.init(port)
	arm.port = port
	nxt.OutputSetRegulation(port, 1, 1)
	nxt.OutputSetSpeed(port, 0x20, -50)
	waitMs(3000)
	nxt.OutputSetSpeed(port)
	nxt.OutputResetTacho(port,1,1,1)
	nxt.OutputSetSpeed(port, 0x20, 50)
	waitMs(3000)
	nxt.OutputSetSpeed(port)
	local _,tacho = nxt.OutputGetStatus(port)
	arm.max_range = tacho
	move(port, -tacho/2 - 90)
	nxt.OutputResetTacho(port,1,1,1)
	arm.tacho = move(port, 90)
end

function arm.rotate(degrees)
	local port = arm.port
	local dest = arm.tacho + degrees
	if dest <= arm.max_range and dest >= 0 then
		local tacho = moveWait(port, 2000)
		moveStart(port, dest - tacho)
		arm.tacho = dest
		return true
	end
	return false
end

function arm.wait()
	local port = arm.port
	moveWait(port, 2000)
end

wheel = {}

function wheel.init(port)
	wheel.port = port
	nxt.OutputResetTacho(port,1,1,1)
	wheel.tacho = 0
end

function wheel.rotate(degrees)
	local port = wheel.port
	local dest = wheel.tacho + degrees
	local tacho = moveWait(port, 2000)
	moveStart(port, dest - tacho)
	wheel.tacho = dest
end

function wheel.wait()
	local port = wheel.port
	moveWait(port, 2000)
end

pen = {}

function pen.init()
	nxt.OutputSetRegulation(1, 1, 1)
	nxt.OutputSetSpeed(1, 0x20, -30)
	waitMs(1000)
	move(1, 140)
	arm.init(2)
	wheel.init(3)
end

function pen.moveXY(dx, dy)
	local alpha = arm.tacho
	local r = nxt.float(82)
	local cos = nxt.cos(alpha) - nxt.float(dx) / r
	local one = nxt.float(1)
	if cos <= one and cos >= -one then
		local da = nxt.acos(cos) - nxt.float(alpha)
		local dl = nxt.float(dy) + r * (nxt.sin(alpha) - nxt.sin(nxt.float(alpha) + da))
		local dwa = (nxt.float(360) * dl) / (nxt.pi() * nxt.float(43, 200000))
		wheel.wait()
		if arm.rotate(nxt.int(da)) then
			wheel.rotate(nxt.int(dwa))
			return true
		end
	end
	return false
end

function pen.wait()
	wheel.wait()
	arm.wait()
end

function pen.up()
	pen.wait()
	move(1, 50)
end

function pen.down()
	pen.wait()
	move(1, -50)
end

font = {
	['1'] = {{0,0}, {1,0}, {0,-2}},
	['2'] = {{0,-1}, {0,1}, {1,0}, {0,-1}, {-1,-1}, {1,0}},
	['3'] = {{0,0}, {1,0}, {0,-1}, {-1,0}, {1,0}, {0,-1}, {-1,0}}
}

function pen.write(str)
	local dx, dy = -70, 0
	local scale = 20
	local down = false
	for i = 1,string.len(str) do
		print('char = ', i, string.sub(str, i, i))
		local pos_x, pos_y = 0, 0
		for _,coord in ipairs(font[string.sub(str, i, i)]) do
			dx = dx + coord[1] * scale
			dy = dy + coord[2] * scale
			print(dx, dy)
			pen.moveXY(dx, dy)
			if not down then
				pen.down()
				down = true
			end
			dx, dy = 0, 0
			pos_x = pos_x + coord[1]
			pos_y = pos_y + coord[2]
		end
		pen.up()
		down = false
		print('pos =', pos_x, pos_y)
		dx = scale * (2 - pos_x)
		dy = scale * (- pos_y)
	end
end

function main()

	pen.init()

	pen.write('123')

	nxt.OutputSetSpeed(1)
	nxt.OutputSetSpeed(2)
	nxt.OutputSetSpeed(3)
end

main()