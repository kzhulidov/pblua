#! /usr/bin/env python

import pygame
import lightblue

s = lightblue.socket()
s.connect(('00:16:53:0C:2B:59', 1))

pygame.init()
pygame.joystick.init()

js = pygame.joystick.Joystick(0)
js.init()

done = False
nom_x = js.get_axis(0)
nom_y = js.get_axis(1)
while not done:
	s.send('!!! %+0.6f %+0.6f %d' % (js.get_axis(0)-nom_x, js.get_axis(1)-nom_y, js.get_button(0) and 1 or 0))
	for event in pygame.event.get():
		if event == pygame.KEYDOWN:
			done = True

