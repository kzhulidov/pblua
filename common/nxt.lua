-- nxt.lua

nxt = {}

nxt.bt_status = 4
nxt.pi = function() return math.pi end

nxt.out = {}
nxt.out[1] = {}
nxt.out[2] = {}
nxt.out[3] = {}

function nxt.TimerRead()
	return os.time() * 1000
end

function nxt.BtGetStatus()
	return nxt.bt_status, 17
end

function nxt.BtPower(val)
	if val == 0 then
		nxt.bt_status = 4
	else
		nxt.bt_status = 1
	end
end

function nxt.BtSetPIN()
	nxt.bt_status = 2
end

function nxt.BtStreamMode()
end

function nxt.ButtonRead()
	return 0
end

function nxt.OutputResetTacho(port)
	nxt.out[port].tacho = 0
end

function nxt.OutputSetRegulation()
end

function nxt.OutputSetSpeed(port, _, speed, tacho)
	local speed = speed or 0
	nxt.out[port].speed = speed
	local tacho = tacho or 180
	local oldtacho = nxt.out[port].tacho or 0
	nxt.out[port].tacho = oldtacho + nxt.sign(speed) * tacho
end

function nxt.OutputGetStatus(port)
	return 0, nxt.out[port].tacho, 0, 0, 0, 0, 0
end

function nxt.band(num1, num2)
	local r = 0
	for i = 0,31 do
		r = r + (2 ^ i) * (math.floor(num1 / (2 ^ i)) % 2) * (math.floor(num2 / (2 ^ i)) % 2)
	end
	return r
end

function nxt.floor(num)
	return math.floor(num)
end

function nxt.int(num)
	if num then
		local sign = nxt.sign(num)
		num = math.abs(num)
		local int = math.floor(num)
		return int * sign, math.floor((num - int) * 1000000) * sign
	end
end

function nxt.sign(num)
	if num then
		local r = 0
		if num < 0 then
			r = -1
		elseif num > 0 then
			r = 1
		end
		return r
	end
end

function nxt.float(int, exp)
	local int = int or 0
	local exp = exp or 0
	return int + exp / 1000000
end

function nxt.sin(alpha)
	return math.sin(math.rad(alpha))
end

function nxt.cos(alpha)
	return math.cos(math.rad(alpha))
end

function nxt.asin(num)
	return math.deg(math.asin(num))
end

function nxt.acos(num)
	return math.deg(math.acos(num))
end

function nxt.abs(num)
	return math.abs(num)
end
