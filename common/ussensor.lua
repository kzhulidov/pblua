-- ussensor.lua

require('common.sensor')


USSensor = class(Sensor)

function waitI2C(port)
	repeat
		--coroutine.yield()
	until 0 == nxt.I2CGetStatus(port)
end

function setupUltrasonic(port)

	nxt.InputSetType(port, 0)
	nxt.InputSetType(port, 11)
	nxt.InputSetDir(port, 1, 1)
	nxt.InputSetState(port, 1, 1)

	nxt.I2CInitPins(port)

	-- Now put the standard I2C messages into the nxt table
	nxt.I2Cversion    = string.char(0x02, 0x00)
	nxt.I2Cproduct    = string.char(0x02, 0x08)
	nxt.I2Ctype       = string.char(0x02, 0x10)
	nxt.I2Ccontinuous = string.char(0x02, 0x41, 0x02)
	nxt.I2Cdata       = string.char(0x02, 0x42)

	--nxt.I2CSendData(port, nxt.I2Ccontinuous, 0)
	--waitI2C(port)

end

function getUltrasonicData(port)

	nxt.I2CSendData(port, string.char(0x02, 0x41, 0x01), 0)
	waitI2C(port)
	waitMs(50)

	nxt.I2CSendData(port, nxt.I2Cdata, 8)
	waitI2C(port)
	waitMs(10)

	local s = nxt.I2CRecvData(port, 8)
	return string.byte(s, 1, 8)

end