#! /usr/bin/env python

import serial
import sys
import time

port = sys.argv[1]
baudrate = 115200
timeout = 1

loaderInst = """
function xmodemLoader()
	local file = ''
	local str
	repeat
		str = nxt.XMODEMRecv()
		if str then
			file = file .. str
		end
	until nil == str
	return file
end
xmodemLoaderFunc = loadstring(xmodemLoader())
"""

loaderUninst = """
xmodemLoader = nil
xmodemLoaderFunc()
xmodemLoaderFunc = nil
collectgarbage()
"""

def xmodem_send(serial, file):

	def calc_crc(s):
		crc = 0
		for c in s:
			crc = crc ^ (ord(c) << 8)
			for i in range(8):
				if crc & 0x8000:
					crc = crc << 1 ^ 0x1021
				else:
					crc = crc << 1
		return crc & 0xFFFF

	NAK = 0x15
	ACK = 0x06
	SOH = '\x01'
	EOT = '\x04'

	p = 1
	s = file.read(128)
	while s:
		s = s + ' '*(128 - len(s))
		crc = calc_crc(s)
		while 1:
			serial.write(SOH)
			serial.write(chr(p))
			serial.write(chr(255 - p))
			serial.write(s)
			serial.write(chr(crc >> 8))
			serial.write(chr(crc & 0xFF))
			serial.flush()

			answer = serial.read(1)
			if ord(answer) == NAK:
				continue
			elif ord(answer) == ACK:
				break
			else:
				print answer
				return False

		s = file.read(128)
		p = (p + 1)%256
		print '.',

	serial.write(EOT)
	print
	return True

def install_loader(serial):
	serial.flushInput()
	serial.write(loaderInst)
	serial.flush()
	print 'Waiting for receiver ...'
	while 1:
		c = serial.read(1)
		if c == 'C': break

def uninstall_loader(serial):
	serial.write(loaderUninst)
	serial.flush()

def is_responsive(serial):
	serial.flushInput()
	serial.write('\n')
	serial.flush()
	return '\r\n> ' == serial.read(4)

ser = serial.Serial(port, baudrate = baudrate, timeout = timeout)

if is_responsive(ser):
	print 'Installing loader ...'
	install_loader(ser)
	print 'Uploading ...'
	if xmodem_send(ser, open(sys.argv[2], 'r')):
		print '-> OK'
	else:
		print '-> failed'
	time.sleep(2)
	print 'Uninstalling loader ...'
	uninstall_loader(ser)
	time.sleep(2)
	ser.flushInput()
else:
	print 'Not responsive, cannot upload'
